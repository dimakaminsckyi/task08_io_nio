package com.epam.othertask.controller;

public interface BufferTestController {

    void writeToFile();

    void readFromFile();

    void writeToFileWithBuffer();

    void readFromFileWithBuffer();
}
