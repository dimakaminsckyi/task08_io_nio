package com.epam.othertask.controller;

public interface DirectoryController {

    void showFilesInDirectoryRecursive();

    void showFilesInDirectory();

    void deleteDirectory();

    void createDirectory();

    void moveToDirectory();
}
