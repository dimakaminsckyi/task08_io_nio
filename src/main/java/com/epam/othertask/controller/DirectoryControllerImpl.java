package com.epam.othertask.controller;

import com.epam.othertask.model.DirectoryContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class DirectoryControllerImpl implements DirectoryController{

    private static Logger log = LogManager.getLogger(DirectoryControllerImpl.class);
    private DirectoryContent directoryContent;
    private Scanner scanner = new Scanner(System.in);

    public DirectoryControllerImpl() {
        directoryContent = new DirectoryContent();
    }

    @Override
    public void showFilesInDirectoryRecursive() {
        directoryContent.listFilesInDirTreeRecursive();
    }

    @Override
    public void showFilesInDirectory() {
        directoryContent.showFileInDirectory();
    }

    @Override
    public void deleteDirectory() {
        print();
        directoryContent.deleteDirectory(scanner.nextLine());
    }

    @Override
    public void createDirectory() {
        print();
        directoryContent.createDirectory(scanner.nextLine());
    }

    @Override
    public void moveToDirectory() {
        print();
        directoryContent.moveToDirectory(scanner.nextLine());
    }

    private void print(){
        log.info("Enter directory : ");
    }
}
