package com.epam.othertask.controller;

import com.epam.othertask.model.BufferTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class BufferTestControllerImpl implements BufferTestController {

    private Logger log = LogManager.getLogger(BufferTestControllerImpl.class);
    private BufferTest bufferTest;
    private Scanner scanner = new Scanner(System.in);

    public BufferTestControllerImpl() {
        bufferTest = new BufferTest();
    }

    @Override
    public void writeToFile() {
        log.info("Input megabytes : ");
        log.info("Milliseconds : " + bufferTest.writeToFile(scanner.nextInt()) + "\n");
    }

    @Override
    public void readFromFile() {
        log.info("Milliseconds : " + bufferTest.readFromFile());
    }

    @Override
    public void writeToFileWithBuffer() {
        log.info("Input megabytes : ");
        log.info("Milliseconds : " + bufferTest.writeToFileWithBuffer(scanner.nextInt()) + "\n");
    }

    @Override
    public void readFromFileWithBuffer() {
        log.info("Input megabytes : ");
        log.info("Milliseconds : " + bufferTest.readFromFileWithBuffer(scanner.nextInt()) + "\n");
    }
}
