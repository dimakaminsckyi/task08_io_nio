package com.epam.othertask.view;

import com.epam.othertask.controller.DirectoryController;
import com.epam.othertask.controller.DirectoryControllerImpl;
import com.epam.othertask.view.util.MenuUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class DirectoryView {
    private static Logger log = LogManager.getLogger(BufferView.class);
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public DirectoryView() {
        initMenu();
        DirectoryController directoryController = new DirectoryControllerImpl();
        methodRun = new LinkedHashMap<>();
        methodRun.put("show", directoryController::showFilesInDirectory);
        methodRun.put("showRecursive", directoryController::showFilesInDirectoryRecursive);
        methodRun.put("cd", directoryController::moveToDirectory);
        methodRun.put("create", directoryController::createDirectory);
        methodRun.put("delete", directoryController::deleteDirectory);
        methodRun.put("return", () -> log.info("Returned"));

    }

    public void show() {
        MenuUtil.runMenu(methodRun, menu);
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("show", "show - Show files in directory");
        menu.put("showRecursive", "showRecursive - Show tree directory");
        menu.put("cd", "cd - Move to directory");
        menu.put("create", "create - Create directory");
        menu.put("delete", "delete - Delete Directory");
        menu.put("return", "return - Return");
    }
}
