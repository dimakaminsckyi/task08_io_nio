package com.epam.othertask.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private BufferView bufferView;
    private DirectoryView directoryView;
    private List<String> menuList;

    public MainView() {
        bufferView = new BufferView();
        directoryView = new DirectoryView();
        menuList = new LinkedList<>();
        menuList.add("directory - Show directory task menu");
        menuList.add("buffer - Show buffer task menu");
        menuList.add("exit - Exit");
    }

    public void showView(){
        Scanner scanner = new Scanner(System.in);
        StringBuilder stringBuilder = new StringBuilder();
        while(!stringBuilder.toString().equals("exit")){
            menuList.forEach(log::info);
            stringBuilder.append(scanner.next());
            switch (stringBuilder.toString()){
                case "directory":
                    directoryView.show();
                    stringBuilder.setLength(0);
                    break;
                case "buffer":
                    bufferView.show();
                    stringBuilder.setLength(0);
                    break;
                case "exit":
                    stringBuilder.setLength(0);
                    stringBuilder.append("exit");
                    break;
                default:
                    log.warn("Enter correct input");
            }
        }
        scanner.close();
    }
}
