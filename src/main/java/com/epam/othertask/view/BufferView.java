package com.epam.othertask.view;

import com.epam.othertask.controller.BufferTestController;
import com.epam.othertask.controller.BufferTestControllerImpl;
import com.epam.othertask.view.util.MenuUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class BufferView {


    private static Logger log = LogManager.getLogger(BufferView.class);
    private BufferTestController buffer = new BufferTestControllerImpl();
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public BufferView() {
        initMenu();
        methodRun = new LinkedHashMap<>();
        methodRun.put("read", () -> buffer.readFromFile());
        methodRun.put("write", () -> buffer.writeToFile());
        methodRun.put("readBuffer" , () -> buffer.readFromFileWithBuffer());
        methodRun.put("writeBuffer", () -> buffer.writeToFileWithBuffer());
        methodRun.put("return", () -> log.info("Returned"));
    }

    public void show() {
        MenuUtil.runMenu(methodRun, menu);
    }

    private void initMenu(){
        menu = new LinkedHashMap<>();
        menu.put("writeBuffer", "writeBuffer - Write data to file with buffer");
        menu.put("readBuffer", "readBuffer - Read from file with Buffer");
        menu.put("write" , "write - Write data to file");
        menu.put("read", "read - Read From file (near 4 minutes)");
        menu.put("return", "return - Return");
    }
}
