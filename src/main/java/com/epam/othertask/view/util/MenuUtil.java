package com.epam.othertask.view.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Scanner;

public class MenuUtil {

    private static Logger log = LogManager.getLogger(MenuUtil.class);

    public static void runMenu(Map<String, Runnable> methodRun, Map<String, String> menu) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder flagExit = new StringBuilder("");
        do {
            menu.values().forEach(log::info);
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.warn("Input correct option");
            }
        } while (!flagExit.toString().equals("return"));
    }
}
