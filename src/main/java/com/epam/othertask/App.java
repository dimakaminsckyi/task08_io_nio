package com.epam.othertask;

import com.epam.othertask.view.MainView;

public class App {
    public static void main(String[] args) {
        new MainView().showView();
    }
}
