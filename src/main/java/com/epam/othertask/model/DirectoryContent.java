package com.epam.othertask.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.*;

public class DirectoryContent {

    private static Logger log = LogManager.getLogger(DirectoryContent.class);
    private Path dirName = FileSystems.getDefault().getPath("nioTestFolder").toAbsolutePath();

    public void listFilesInDirTreeRecursive() {
        try {
            Files.walk(Paths.get(dirName.toUri()))
                    .skip(1)
                    .map(Path::getFileName)
                    .forEach(log::info);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void showFileInDirectory() {
        try {
            Files.list(Paths.get(dirName.toUri()))
                    .map(Path::getFileName)
                    .forEach(log::info);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void moveToDirectory(String directory) {
        String oldFolder = dirName.getFileName().toString() + "/";
        dirName = FileSystems.getDefault().getPath(oldFolder + directory).toAbsolutePath();
        showFileInDirectory();

    }

    public void createDirectory(String directoryName) {
        try {
            Path newPath = Paths.get(dirName.toAbsolutePath().toString() + "/" + directoryName);
            if (Files.exists(newPath)) {
                log.warn("Directory Already exists");
            } else {
                Files.createDirectory(newPath);
                log.info("Created");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

    }

    public void deleteDirectory(String directoryName) {
        try {
            Path newPath = Paths.get(dirName.toAbsolutePath().toString() + "/" + directoryName);
            if (Files.deleteIfExists(newPath)) {
                log.info("Deleted");
            } else {
                log.warn("Directory not found");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
