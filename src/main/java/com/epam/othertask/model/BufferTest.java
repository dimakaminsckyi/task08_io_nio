package com.epam.othertask.model;

import java.io.*;

public class BufferTest {

    private int megabyte = 1024 * 1024;
    private final String FILE_NAME = "someFile.dat";

    public long writeToFileWithBuffer(int megabytesNumber){
        long start = System.currentTimeMillis();
        try(DataOutputStream out = new DataOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream(FILE_NAME), megabyte * megabytesNumber))) {
            out.write(createDataSize(megabytesNumber));
            } catch (IOException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    public long readFromFileWithBuffer(int megabytesNumber){
        long start = System.currentTimeMillis();
        try(DataInputStream input = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(FILE_NAME), megabyte * megabytesNumber))) {
            int data = input.read();
            while (data != -1){
                data = input.read();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    public long writeToFile(int megabytesNumber){
        long start = System.currentTimeMillis();
        try(FileOutputStream output = new FileOutputStream(FILE_NAME)){
            output.write(createDataSize(megabytesNumber));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    public long readFromFile(){
        long start = System.currentTimeMillis();
        try(FileInputStream input = new FileInputStream(FILE_NAME)){
            int data = input.read();
            while (data != -1){
                data = input.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    private byte[] createDataSize(int bytesArraySize){
        byte[] bytes = new byte[megabyte * bytesArraySize];
        for (int i = 0;i < bytes.length; i++){
            bytes[i] = 1;
        }
        return bytes;
    }
}
